#ifndef _matrix_h_included_
#define _matrix_h_included_

struct matrix_image {
  /* number of 'int elements */
  int size;
  /* array of 'int */
  int *image;
};
struct sparse_matrix;
struct sparse_matrix *anthy_sparse_matrix_new(void);
void anthy_sparse_matrix_set(struct sparse_matrix *m, int row, int column, int value, void *ptr);
void anthy_sparse_matrix_make_matrix(struct sparse_matrix *m);
struct matrix_image *anthy_matrix_image_new(struct sparse_matrix *s);
int anthy_matrix_peek(int *im, int row, int col);

#endif
