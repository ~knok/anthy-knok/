#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <anthy.h>

#define GENE_NUM 100

struct test_data {
  int size;
  struct sentence {
    char* sound;
    int nr_segments;
    int* segment_sizes;
  }* data;
};
anthy_context_t ac;
struct test_data tdata;

struct gene {
  float score;
  float normal_length;
  int score_per_len;
  int score_per_freq;
  int score_per_depword;
};

static void
setup_test_data(const char* filename)
{
  FILE* fp;
  char buffer[256];
  int size;
  int i, j;
  memset(buffer, 0, sizeof(char) * 256);
  
  fp = fopen(filename, "r");

  fgets(buffer, 256, fp);
  sscanf(buffer, "%d", &size);
  tdata.size = size;
  tdata.data = (struct sentence*)calloc(sizeof(struct sentence), size);

  for (i = 0; i < size; ++i) {
    char* token;
    struct sentence* datum = &tdata.data[i];
    int nr_segments;
    fgets(buffer, 256, fp);
    token = strtok(buffer, " \n");
    datum->sound = (char*)malloc(sizeof(char) * (strlen(token) + 1));
    strcpy(datum->sound, token);

    token = strtok(NULL, " \n");
    sscanf(token, "%d", &nr_segments);
    datum->nr_segments = nr_segments;
    datum->segment_sizes = (int**)malloc(sizeof(int) * nr_segments);
    for (j = 0; j < nr_segments; ++j) {
      token = strtok(NULL, " \n");
      datum->segment_sizes[j] = strlen(token);
    }
  }

  fclose(fp);
}

static float
eval_gene(struct gene* gene)
{
  int i, j;
  int success = 0;
  int failure = 0;
  anthy_set_normal_length(gene->normal_length);
  anthy_set_score_per_len(gene->score_per_len);
  anthy_set_score_per_freq(gene->score_per_freq);
  anthy_set_score_per_depword(gene->score_per_depword);

  for (i = 0; i < tdata.size; ++i) {
    int nr_segments;
    struct anthy_conv_stat cs;
    anthy_set_string(ac, tdata.data[i].sound);
    anthy_get_stat(ac, &cs);
    nr_segments = cs.nr_segment;
    if (nr_segments != tdata.data[i].nr_segments) {
      ++failure;
      continue;
    }
    for (j = 0; j < nr_segments; ++j) {
      int seg_len;
      seg_len = anthy_get_segment(ac, j, NTH_UNCONVERTED_CANDIDATE, NULL, 0);
      if (seg_len  != tdata.data[i].segment_sizes[j]) {
	++failure;
	break;
      }
    }
    if (j == nr_segments) {
      ++success;
    }
  }
  printf("eval..done\n");
  return (float)success / (success + failure);
}

static void
create_first_gene(struct gene* first_gene)
{
  first_gene->normal_length = (float)(rand() % 10000) / 100;
  first_gene->score_per_len = rand() % 10000;
  first_gene->score_per_freq = rand() % 10000;
  first_gene->score_per_depword = rand() % 10000;
  first_gene->score = eval_gene(first_gene);
  printf("%f, %f, %d, %d, %d\n", first_gene->score, first_gene->normal_length, first_gene->score_per_len, first_gene->score_per_freq, first_gene->score_per_depword);
}

static void
create_child_gene(struct gene* child, struct gene* mother, struct gene* father)
{
  int split_point = rand() % (sizeof(struct gene));;
  memcpy(child, mother, split_point);
  memcpy((char*)child + split_point, (char*)father + split_point, sizeof(struct gene) - split_point);
  if (rand() % 100 < 5) {
    *((char*)child + (rand() % sizeof(struct gene))) = rand() % sizeof(char);
  }

  child->score = eval_gene(child);
  printf("%f, %f, %d, %d, %d\n", child->score, child->normal_length, child->score_per_len, child->score_per_freq, child->score_per_depword);
}

int gene_cmp(const void* lhs, const void* rhs)
{
  struct gene* lgene = (struct gene*)(lhs);
  struct gene* rgene = (struct gene*)(rhs);

  if (lgene->score < rgene->score) {
    return 1;
  } else if (lgene->score > rgene->score) {
    return -1;
  } else {
    return 0;
  }
}

void alter_generation(int gene_size, struct gene* genes)
{
  int i;
  qsort(genes, gene_size, sizeof(struct gene), gene_cmp);
  for (i = gene_size >> 1; i < gene_size; ++i) {
    create_child_gene(&genes[i], 
		      &genes[i - (gene_size >> 1)],
		      &genes[i - (gene_size >> 1) + 1]);
  }
  printf("%f, %f, %d, %d, %d\n", genes[0].score, genes[0].normal_length, genes[0].score_per_len, genes[0].score_per_freq, genes[0].score_per_depword);
}

static void
init_lib(void)
{
  /* $B4{$K%$%s%9%H!<%k$5$l$F$$$k%U%!%$%k$N1F6A$r<u$1$J$$$h$&$K$9$k(B */
  anthy_conf_override("CONFFILE", "../anthy-conf");
  anthy_conf_override("HOME", ".");
  anthy_conf_override("DEPWORD", "master.depword");
  anthy_conf_override("PARAMETER", "../src-util/parameter.txt");
  anthy_conf_override("DIC_FILE", "../mkanthydic/anthy.dic");
  anthy_conf_override("ANTHYDIR", "../depgraph");

}



int
main(int argc, char **argv)
{
  int i;
  struct gene genes[GENE_NUM];
  init_lib();
  srand(time());

  if (anthy_init()) {
    printf("failed to init anthy\n");
    exit(0);
  }
  anthy_context_set_encoding(ac, ANTHY_EUC_JP_ENCODING);  
  anthy_set_personality("");
  ac = anthy_create_context();


  setup_test_data("./test.dat");
  for (i = 0; i < GENE_NUM; ++i) {
    create_first_gene(&genes[i]);
  }

  for (i = 0; i < 100; ++i) {
    alter_generation(GENE_NUM, genes);
  }

  anthy_release_context(ac);
  anthy_quit();

  return 0;
}
